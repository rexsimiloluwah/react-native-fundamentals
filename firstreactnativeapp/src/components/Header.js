import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default function Header({ title, color }) {
    return (
        <View style={[{ backgroundColor: color }, styles.header]}>
            <Text style={styles.headerText}>{title}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        paddingVertical: 20,
    },
    headerText: {
        color: '#fff',
        fontSize: 24,
        fontWeight: 'bold',
    },
});
