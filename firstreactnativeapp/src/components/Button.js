import React from 'react';
import { Pressable, Text, StyleSheet } from 'react-native';
export default function Button({ handlePress, label, color, style }) {
    return (
        <Pressable
            onPress={handlePress} // Using long press, change to onLongPress
            delayLongPress={1500}
            style={({ pressed }) => [
                { backgroundColor: pressed ? 'crimson' : color },
                styles.button,
                { ...style },
            ]}
            // disabled={submitted}
            android_ripple={{ color: 'magenta' }}
            hitSlop={{ top: 10, bottom: 10, right: 10, left: 10 }} //offset for clicking around the button
        >
            <Text style={styles.buttonText}>{label}</Text>
        </Pressable>
    );
}

const styles = StyleSheet.create({
    button: {
        color: '#fff',
        paddingVertical: 15,
        paddingHorizontal: 32,
        marginVertical: 10,
        borderRadius: 12,
        alignItems: 'center',
    },
    buttonText: {
        color: '#ffffff',
        fontSize: 24,
    },
});
