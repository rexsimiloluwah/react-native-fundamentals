import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import globalStyles from '../styles/global';

export default function Map({ route }) {
    const { city } = route.params;

    return (
        <View style={globalStyles.Body}>
            <Text style={globalStyles.WildText}>{city}</Text>
        </View>
    );
}

const styles = StyleSheet.create({});
