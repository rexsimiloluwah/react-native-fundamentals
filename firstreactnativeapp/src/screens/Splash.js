import React, { useEffect } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import globalStyles from '../styles/global';
import PushNotification from 'react-native-push-notification';

const Splash = ({ navigation }) => {
    const createChannel = () => {
        PushNotification.createChannel({
            channelId: 'task-channel',
            channelName: 'Test channel',
        });
    };

    useEffect(() => {
        createChannel();
        // AsyncStorage.clear()
        // for navigating to the home screen after 2 seconds
        setTimeout(() => {
            navigation.replace('Tasks');
        }, 5000);
    }, []);

    return (
        <View style={[styles.body, globalStyles.Body]}>
            <Image style={styles.logo} source={require('../../assets/Calendar.png')} />
            <Text style={styles.splashTitle}>Tasky</Text>
            <Text style={styles.splashText}>Just another task manager!</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    body: {
        backgroundColor: '#5a8ad3',
        color: '#ffffff',
    },
    logo: {
        width: 150,
        height: 196,
    },
    splashTitle: {
        color: '#ffffff',
        fontSize: 40,
        marginBottom: 16,
        fontWeight: '500',
        fontFamily: 'Finlandica-Bold',
    },
    splashText: {
        color: '#ffffff94',
        fontSize: 24,
        fontFamily: 'Finlandica-Medium',
    },
});

export default Splash;
