import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Image,
    Alert,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import Button from '../components/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector, useDispatch } from 'react-redux';
import { updateTask } from '../redux/todoSlice';
import CheckBox from '@react-native-community/checkbox';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const UpdateTask = ({ navigation }) => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [isDone, setIsDone] = useState(false);
    const [color, setColor] = useState('');
    const [image, setImage] = useState('');
    const dispatch = useDispatch();
    const { tasks, taskId } = useSelector((state) => state.todos);
    const [isInputFocused, setIsInputFocused] = useState({
        title: false,
        description: false,
    });

    const COLORS = ['springgreen', 'crimson', '#5a8ad3', 'orange'];

    const handleInputFocus = (name) => {
        setIsInputFocused({
            [name]: true,
        });
    };

    const getCurrentTask = () => {
        const currentTask = tasks.find((task) => task.id === taskId);
        console.log('Current task: ', currentTask);
        if (currentTask) {
            setTitle(currentTask.title);
            setDescription(currentTask.description);
            setIsDone(currentTask.done);
            setColor(currentTask.color);
            setImage(currentTask.image);
        }
    };

    useEffect(() => {
        navigation.addListener('focus', () => {
            getCurrentTask();
        });
    }, [tasks]);

    const handleUpdateTask = () => {
        if (title.length === 0) {
            Alert.alert('Error!', 'Title is required');
            return;
        }

        try {
            const updatedTask = {
                id: taskId,
                title: title,
                description: description,
                done: isDone,
                color: color,
                image: image,
            };
            const taskIdx = tasks.findIndex((task) => task.id === taskId);
            dispatch(updateTask({ id: taskIdx, data: updatedTask }));
            let newTasks = [...tasks];
            newTasks[taskIdx] = updatedTask;
            AsyncStorage.setItem('Tasks', JSON.stringify(newTasks)).then(() => {
                Alert.alert('Success!', 'Task updated successfully.', [
                    { text: 'OK', onPress: () => navigation.goBack() },
                ]);
            });
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <ScrollView>
            <View style={styles.form}>
                <Image style={styles.logo} source={require('../../assets/House.png')} />
                <TextInput
                    style={
                        isInputFocused.title
                            ? [styles.input, { borderColor: '#5a8ad3', borderWidth: 3 }]
                            : styles.input
                    }
                    placeholder="Task Title"
                    onFocus={() => handleInputFocus('title')}
                    onChangeText={(value) => setTitle(value)}
                    value={title}
                />
                <TextInput
                    style={
                        isInputFocused.description
                            ? [styles.input, { borderColor: '#5a8ad3', borderWidth: 3 }]
                            : styles.input
                    }
                    placeholder="Task Description"
                    onFocus={() => handleInputFocus('description')}
                    onChangeText={(value) => setDescription(value)}
                    value={description}
                    multiline
                />
                <TouchableOpacity
                    style={styles.cameraButton}
                    onPress={() => navigation.navigate('Camera', { taskId: taskId })}
                >
                    <MaterialCommunityIcons name="camera" size={32} color="#ffffff" />
                </TouchableOpacity>
                <View style={styles.checkbox}>
                    <Text style={styles.text}>Is Done</Text>
                    <CheckBox
                        disabled={false}
                        value={isDone}
                        onValueChange={(value) => setIsDone(value)}
                    />
                </View>
                <View style={styles.colors}>
                    {COLORS.map((value, index) => (
                        <TouchableOpacity
                            key={index}
                            style={[
                                {
                                    backgroundColor: value,
                                    flex: 1,
                                    height: 50,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                },
                            ]}
                            onPress={() => setColor(value)}
                        >
                            {color === value ? (
                                <MaterialCommunityIcons
                                    name="check-bold"
                                    color="#ffffff"
                                    size={32}
                                />
                            ) : (
                                ''
                            )}
                        </TouchableOpacity>
                    ))}
                </View>
                {image ? (
                    <View>
                        <Image source={{ uri: image }} style={styles.image} />
                    </View>
                ) : (
                    ''
                )}

                <Button
                    label="Update Task"
                    color="#5a8ad3"
                    style={{ width: '100%' }}
                    handlePress={() => handleUpdateTask()}
                />
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    form: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 18,
    },
    input: {
        width: '100%',
        borderWidth: 2,
        borderColor: '#00000050',
        backgroundColor: '#ffffff',
        fontSize: 24,
        marginVertical: 12,
        padding: 16,
        borderRadius: 12,
    },
    logo: {
        width: 120,
        height: 144,
    },
    checkbox: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
    },
    text: {
        color: '#000000',
        fontSize: 18,
    },
    colors: {
        flexDirection: 'row',
        marginVertical: 5,
    },
    cameraButton: {
        backgroundColor: '#5a8ad3',
        padding: 12,
        borderRadius: 12,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    image: {
        width: 200,
        height: 200,
    },
});

export default UpdateTask;
