import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, Alert, TouchableOpacity } from 'react-native';
import globalStyles from '../styles/global';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Button from '../components/Button';
import { FlatList, TextInput } from 'react-native-gesture-handler';
import SQLite from 'react-native-sqlite-storage';
import { useSelector, useDispatch } from 'react-redux';
import { setEmail, setPassword } from '../redux/userSlice';
import { incrementCount, decrementCount } from '../redux/countSlice';
import { fetchCities } from '../redux/citiesSlice';
import PushNotification from 'react-native-push-notification';

// Initialize the database
const db = SQLite.openDatabase(
    {
        name: 'MainDB',
        location: 'default',
    },
    () => {},
    (error) => {
        console.error(error);
    }
);

export default function Home({ navigation, route }) {
    // const [userEmail,setUserEmail] = useState("")
    // const [userPassword, setUserPassword] = useState('');
    const { email, password } = useSelector((state) => state.user);
    const { count } = useSelector((state) => state.count);
    const { cities, status, loading, error } = useSelector((state) => state.cities);

    const dispatch = useDispatch();

    useEffect(() => {
        getData();
        console.log(status);
        if (status === 'idle') {
            dispatch(fetchCities());
        }
    }, [status, dispatch]);

    const getData = () => {
        // try{
        //     AsyncStorage.getItem("userData").then(value => {
        //         if(value){
        //             const {email,password} = JSON.parse(value)
        //             setUserEmail(email)
        //             setUserPassword(password)
        //         }
        //     })
        // }catch(error){
        //     console.error(error)
        // }
        db.transaction((tx) => {
            tx.executeSql('SELECT email,password FROM Users', [], (tx, results) => {
                let numRows = results.rows.length;
                if (numRows > 0) {
                    // setUserEmail(results.rows.item(0).email);
                    // setUserPassword(results.rows.item(0).password);
                    dispatch(setEmail(results.rows.item(0).email));
                    dispatch(setPassword(results.rows.item(0).password));
                }
            });
        });
    };

    // Push notification -> on click of items
    const handlePushNotification = (item, index) => {
        // cancel all notifications
        PushNotification.cancelAllLocalNotifications();

        PushNotification.localNotification({
            channelId: 'test-channel',
            title: `You clicked: ${item.country}`,
            message: item.city,
            bigText: `${item.country} is one of the largest and most beautiful countries in the world.`,
            color: 'red',
            id: index,
        });

        // timed notification
        // PushNotification.localNotificationSchedule({
        //     channelId: "test-channel",
        //     title: `Alarm for ${item.country} click event.`,
        //     message: `You clicked on ${item.country} 20 seconds ago.`,
        //     date: new Date(Date.now() + 20*1000),
        //     allowWhileIdle: true,
        // })
    };

    const updateData = async () => {
        if (email.indexOf('@') === -1 || !email.endsWith('.com')) {
            Alert.alert('An error occurred.', 'E-mail is invalid.');
            return;
        }

        try {
            // await AsyncStorage.setItem("userEmail",userEmail)
            // Alert.alert('Success', 'Your email has been updated.');
            db.transaction((tx) => {
                tx.executeSql(
                    'UPDATE Users SET email=?',
                    [email],
                    (tx, results) => {
                        if (results.rowsAffected > 0) {
                            Alert.alert('Success!', 'User updated successfully.');
                        }
                    },
                    (error) => {
                        console.error(error);
                    }
                );
            });
        } catch (error) {
            console.error(error);
        }
    };

    const deleteData = async () => {
        if (email.indexOf('@') === -1 || !email.endsWith('.com')) {
            Alert.alert('An error occurred.', 'E-mail is invalid.');
            return;
        }

        try {
            // await AsyncStorage.removeItem("userEmail")
            // Alert.alert("Success", "Your email has been deleted.")
            // navigation.navigate("Login")
            db.transaction((tx) => {
                tx.executeSql(
                    'DELETE from Users',
                    [],
                    (tx, results) => {
                        if (results.rowsAffected > 0) {
                            navigation.navigate('Login');
                        }
                    },
                    (error) => {
                        console.error(error);
                    }
                );
            });
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <View style={globalStyles.Body}>
            <Text style={styles.welcomeText}>Welcome {email}</Text>
            <Text style={styles.welcomeText}>Password: {password}</Text>
            {/* <Text style={globalStyles.WildText}>{count}</Text>
            <TextInput
                placeholder="E-mail"
                value={email}
                onChangeText={(value) => dispatch(setEmail(value))}
                style={globalStyles.Input}
            />
            <View style={styles.actionButtons}>
                <Button label={'Update E-mail'} color={'dodgerblue'} handlePress={updateData} />
                <Button label={'Delete E-mail'} color={'crimson'} handlePress={deleteData} />
            </View>
            <View style={styles.countButtons}>
                <Button label={'+'} color={'magenta'} handlePress={()=>dispatch(incrementCount(1))} />
                <Button label={'-'} color={'orange'} handlePress={()=>dispatch(decrementCount(1))} />
            </View> */}
            <Button
                label={'Open camera'}
                color={'orange'}
                handlePress={() => navigation.navigate('Camera')}
            />
            {cities.length ? (
                <FlatList
                    data={cities}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (
                        <TouchableOpacity
                            onPress={() => {
                                handlePushNotification(item, index);
                                navigation.navigate('Map', {
                                    city: item.city,
                                });
                            }}
                        >
                            <View style={styles.item}>
                                <Text style={styles.title}>{item.country}</Text>
                                <Text style={styles.subtitle}>{item.city}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                    style={{
                        width: '100%',
                    }}
                />
            ) : (
                <Text>{error}</Text>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    welcomeText: {
        alignContent: 'center',
        fontSize: 24,
    },
    actionButtons: {
        marginVertical: 18,
    },
    countButtons: {
        flexDirection: 'row',
        width: '50%',
        justifyContent: 'space-between',
    },
    item: {
        borderWidth: 2,
        borderColor: '#cccccc',
        padding: 14,
        marginVertical: 12,
        justifyContent: 'center',
        alignItems: 'center',
        color: '#000000',
        backgroundColor: '#fff',
    },
    title: {
        color: '#000000',
        fontSize: 28,
        fontWeight: 'bold',
    },
    subtitle: {
        color: '#00000095',
        fontSize: 18,
    },
});
