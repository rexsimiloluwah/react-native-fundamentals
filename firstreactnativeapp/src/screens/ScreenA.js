import React from 'react';
import { View, Text, Pressable, StyleSheet } from 'react-native';
import globalStyle from '../styles/global';

// Screen A component
const ScreenA = ({ navigation, route }) => {
    const { message } = route.params;
    const onPressHandler = () => {
        navigation.navigate('Screen_B', { itemName: 'Screen B', itemId: 12 }); //using replace, the current screen will be replaced and will exit the stack.
    };
    return (
        <View style={globalStyle.Body}>
            <Text style={[globalStyle.CustomFont, styles.h1]}>Screen A</Text>
            <Pressable style={globalStyle.Button} onPress={onPressHandler}>
                <Text style={globalStyle.ButtonText}>Go to Screen B</Text>
            </Pressable>
            <Text style={styles.text}>{message}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    h1: {
        fontSize: 40,
        color: '#000',
        marginVertical: 12,
    },
    text: {
        fontSize: 24,
        color: '#000',
        fontFamily: 'Finlandica-Bold',
    },
});

export default ScreenA;
