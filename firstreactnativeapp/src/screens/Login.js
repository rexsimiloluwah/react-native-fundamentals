import React, { useState, useEffect } from 'react';
import { View, TextInput, Text, StyleSheet, Alert } from 'react-native';
import globalStyle from '../styles/global';
import Button from '../components/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SQLite from 'react-native-sqlite-storage';
import { useSelector, useDispatch } from 'react-redux';
import { setEmail, setPassword } from '../redux/userSlice';
import PushNotification from 'react-native-push-notification';

// Initialize the database
const db = SQLite.openDatabase(
    {
        name: 'MainDB',
        location: 'default',
    },
    () => {},
    (error) => {
        console.error(error);
    }
);

const Login = ({ navigation }) => {
    const { email, password, count } = useSelector((state) => state.user);
    const dispatch = useDispatch();

    // create table
    const createTable = () => {
        db.transaction((tx) => {
            tx.executeSql(
                'CREATE TABLE IF NOT EXISTS ' +
                    'Users ' +
                    '(ID INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, password TEXT);'
            );
        });
    };

    // Push notification test
    // Notifications are communicated using channels
    const createChannel = () => {
        PushNotification.createChannel({
            channelId: 'test-channel',
            channelName: 'Test channel',
        });
    };

    useEffect(() => {
        createTable();
        getData();
        createChannel();
    }, []);

    const handleLogin = async () => {
        if (email.indexOf('@') === -1 || !email.endsWith('.com')) {
            Alert.alert('Invalid credentials', 'E-mail is invalid.');
            return;
        }
        if (password.length < 8) {
            Alert.alert('Invalid credentials', 'Password must contain at least 8 characters.');
            return;
        }

        // if the credentials are valid
        try {
            // await AsyncStorage.setItem("userEmail", userEmail)
            // const userData = {
            //     email: userEmail,
            //     password: userPassword,
            // };
            // await AsyncStorage.setItem('userData', JSON.stringify(userData));
            await db.transaction(async (tx) => {
                await tx.executeSql(`INSERT INTO Users (email,password) VALUES (?,?)`, [
                    email,
                    password,
                ]);
            });
            navigation.navigate('Home');
        } catch (error) {
            console.error(error);
        }
    };

    const getData = () => {
        // try {
        //     AsyncStorage.getItem('userEmail').then((value) => {
        //         if (value) {
        //             navigation.navigate('Home');
        //         }
        //     });
        // } catch (error) {
        //     return;
        // }
        db.transaction((tx) => {
            tx.executeSql('SELECT email,password FROM Users', [], (tx, results) => {
                let numRows = results.rows.length;
                if (numRows > 0) {
                    navigation.navigate('Home');
                    dispatch(setEmail(results.rows.item(0).email));
                    dispatch(setPassword(results.rows.item(0).password));
                    // setUserEmail(results.rows.item(0).email);
                    // setUserPassword(results.rows.item(0).password);
                }
            });
        });
    };

    return (
        <View style={globalStyle.Body}>
            <View style={styles.loginHeader}>
                <Text style={styles.loginHeaderText}>Log In</Text>
            </View>
            <TextInput
                style={globalStyle.Input}
                placeholder="Email"
                value={email}
                onChangeText={(value) => dispatch(setEmail(value))}
            />
            <TextInput
                style={globalStyle.Input}
                placeholder="Password"
                value={password}
                secureTextEntry
                onChangeText={(value) => dispatch(setPassword(value))}
            />
            <Button
                label={'Log In'}
                handlePress={handleLogin}
                color={'#000'}
                style={{ width: '100%' }}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    loginHeader: {
        alignContent: 'center',
        justifyCenter: 'center',
        paddingVertical: 24,
    },
    loginHeaderText: {
        fontSize: 28,
        fontWeight: 'bold',
        color: '#000',
    },
});

export default Login;
