import React, { useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Alert } from 'react-native';
import globalStyles from '../styles/global';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector, useDispatch } from 'react-redux';
import { setTasks, setTaskId, updateCompleted } from '../redux/todoSlice';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CheckBox from '@react-native-community/checkbox';

const CompletedTasks = ({ navigation }) => {
    const dispatch = useDispatch();
    const { tasks } = useSelector((state) => state.todos);
    // Initial fetch of tasks
    const fetchTasks = () => {
        AsyncStorage.getItem('Tasks')
            .then((tasks) => {
                console.log('tasks: ', tasks);
                if (tasks) {
                    console.log('setting tasks');
                    const data = JSON.parse(tasks);
                    dispatch(setTasks(data));
                    return;
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    // when a task is pressed
    const handlePressTask = (task) => {
        dispatch(setTaskId(task.id));
        navigation.navigate('UpdateTask');
    };

    const handleDeleteTask = (item) => {
        const newTasks = tasks.filter((task) => task.id !== item.id);
        AsyncStorage.setItem('Tasks', JSON.stringify(newTasks))
            .then(() => {
                dispatch(setTasks(newTasks));
                Alert.alert('Success!', 'Task deleted successfully.');
            })
            .catch((error) => {
                console.error(error);
            });
    };

    const handleCheck = (item, value) => {
        const taskIdx = tasks.findIndex((task) => task.id === item.id);
        let newTasks = [...tasks];
        if (taskIdx > -1) {
            dispatch(updateCompleted({ id: taskIdx, value: value }));
            newTasks[taskIdx].done = value;
            AsyncStorage.setItem('Tasks', JSON.stringify(newTasks))
                .then(() => {
                    return;
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    };

    useEffect(() => {
        fetchTasks();
    }, []);

    return (
        <View style={globalStyles.Body}>
            {tasks.length ? (
                <FlatList
                    data={tasks.filter((task) => task.done === true)}
                    renderItem={({ item }) => (
                        <TouchableOpacity onPress={() => handlePressTask(item)}>
                            <View style={styles.task}>
                                <CheckBox
                                    disabled={false}
                                    value={item.done}
                                    onValueChange={(value) => handleCheck(item, value)}
                                />
                                <View style={styles.taskMain}>
                                    <Text style={styles.taskTitle}>{item.title}</Text>
                                    <Text style={styles.taskDescription}>{item.description}</Text>
                                </View>
                                <TouchableOpacity
                                    style={styles.deleteIcon}
                                    onPress={() => handleDeleteTask(item)}
                                >
                                    <MaterialCommunityIcons
                                        name="trash-can"
                                        color="crimson"
                                        size={30}
                                    />
                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>
                    )}
                    style={styles.tasks}
                    keyExtractor={(item, index) => index.toString()}
                />
            ) : (
                ''
            )}
            <TouchableOpacity
                style={styles.createTaskButton}
                onPress={() => navigation.navigate('CreateTask')}
            >
                <MaterialCommunityIcons
                    name="plus"
                    size={42}
                    style={{ fontWeight: 'bold' }}
                    color="#ffffff"
                />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    createTaskButton: {
        width: 72,
        height: 72,
        backgroundColor: '#5a8ad3',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        borderRadius: 50,
        bottom: 20,
        right: 30,
        borderColor: '#00000095',
        color: '#ffffff',
    },
    tasks: {
        padding: 10,
        width: '100%',
    },
    task: {
        backgroundColor: '#ffffff',
        paddingHorizontal: 10,
        paddingVertical: 12,
        width: '100%',
        elevation: 5,
        borderRadius: 10,
        marginVertical: 8,
        position: 'relative',
        flexDirection: 'row',
        alignItems: 'center',
    },
    taskMain: {
        marginHorizontal: 5,
    },
    deleteIcon: {
        position: 'absolute',
        right: 10,
        top: 10,
        height: 50,
    },
    taskTitle: {
        fontSize: 24,
        fontWeight: '500',
        color: '#5a8ad3',
    },
    taskDescription: {
        color: '#00000090',
        fontSize: 18,
    },
});

export default CompletedTasks;
