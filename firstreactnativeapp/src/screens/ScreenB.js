import React from 'react';
import { View, Text, Pressable, StyleSheet } from 'react-native';
import globalStyle from '../styles/global';

// Screen B component
const ScreenB = ({ route, navigation }) => {
    // fetching the data passed between screens
    const { itemName, itemId } = route.params;

    const onPressHandler = () => {
        navigation.navigate('Screen_A', { message: 'From Screen B.' });
    };

    return (
        <View style={styles.body}>
            <Text style={styles.h1}>Screen B</Text>
            <Pressable style={globalStyle.Button} onPress={onPressHandler}>
                <Text style={globalStyle.ButtonText}>Go Back</Text>
            </Pressable>
            <Text style={styles.text}>{itemName}</Text>
            <Text style={styles.text}>{itemId}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    h1: {
        fontSize: 40,
        fontWeight: 'bold',
        color: '#000',
        marginVertical: 12,
    },
    text: {
        fontSize: 24,
        fontWeight: '400',
        color: '#000',
    },
    button: {
        backgroundColor: '#000000',
        borderRadius: 12,
        padding: 18,
    },
    buttonText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '500',
    },
});

export default ScreenB;
