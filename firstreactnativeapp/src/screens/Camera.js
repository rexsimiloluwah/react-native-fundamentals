import React from 'react';
import { View, StyleSheet } from 'react-native';
import globalStyles from '../styles/global';
import { RNCamera } from 'react-native-camera';
// react-native-camera-hooks
import { useCamera } from 'react-native-camera-hooks';
import Button from '../components/Button';
import RNFS from 'react-native-fs';
import { useSelector, useDispatch } from 'react-redux';
import { updateTaskImageUri } from '../redux/todoSlice';

export default function Camera({ navigation, route }) {
    const [{ cameraRef }, { takePicture }] = useCamera(null);
    const { taskId } = route.params;
    const { tasks } = useSelector((state) => state.todos);
    const dispatch = useDispatch();

    const updateTaskImage = (id, path) => {
        const taskIdx = tasks.findIndex((task) => task.id === id);
        dispatch(updateTaskImageUri({ id: taskIdx, path: path }));
        navigation.goBack();
    };

    const handleCapture = async () => {
        try {
            const data = await takePicture();
            console.log(data.uri);
            console.log('Task ID', taskId);
            updateTaskImage(taskId, data.uri);
            // to save the captured image to a destination
            const newFilePath =
                RNFS.ExternalDirectoryPath + `/${new Date().getTime().toString()}.jpg`;
            RNFS.moveFile(data.uri, newFilePath)
                .then(() => {
                    console.log(`Image moved from ${data.uri} to ${newFilePath}`);
                })
                .catch((error) => {
                    console.error(error);
                });
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <View style={globalStyles.Body}>
            <RNCamera
                ref={cameraRef}
                type={RNCamera.Constants.Type.back} // camera position - front|back
                style={styles.cameraPreview}
            >
                <Button label="Capture" color="crimson" handlePress={() => handleCapture()} />
            </RNCamera>
        </View>
    );
}

const styles = StyleSheet.create({
    cameraPreview: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
});
