import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Modal,
    Image,
    Alert,
    TouchableOpacity,
} from 'react-native';
import Button from '../components/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector, useDispatch } from 'react-redux';
import { setTasks } from '../redux/todoSlice';
import globalStyles from '../styles/global';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import PushNotification from 'react-native-push-notification';

const CreateTask = ({ navigation }) => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [color, setColor] = useState('springgreen');
    const [showScheduleModal, setShowScheduleModal] = useState(false);
    const [scheduleMinutes, setScheduleMinutes] = useState(null);
    const dispatch = useDispatch();
    const { tasks } = useSelector((state) => state.todos);
    const [isInputFocused, setIsInputFocused] = useState({
        title: false,
        description: false,
    });

    const COLORS = ['springgreen', 'crimson', '#5a8ad3', 'orange'];

    const handleInputFocus = (name) => {
        setIsInputFocused({
            [name]: true,
        });
    };

    const handleCreateTask = () => {
        if (title.length === 0) {
            Alert.alert('Error!', 'Title is required');
            return;
        }

        try {
            let taskId = tasks.length + 1;
            const newTask = {
                id: taskId,
                title: title,
                description: description,
                color: color,
                done: false,
                image: '',
            };
            const newTasks = [...tasks, newTask];
            console.log('new tasks: ', newTasks);
            AsyncStorage.setItem('Tasks', JSON.stringify(newTasks)).then(() => {
                dispatch(setTasks(newTasks));
                Alert.alert('Success!', 'Task created successfully.', [
                    { text: 'OK', onPress: () => navigation.goBack() },
                ]);
            });
        } catch (error) {
            console.error(error);
        }
    };

    // for setting the schedule
    const addScheduleAlarm = (title, description) => {
        PushNotification.localNotificationSchedule({
            channelId: 'task-channel',
            title: title,
            message: description,
            date: new Date(Date.now() + parseInt(scheduleMinutes) * 60 * 1000),
        });
    };
    return (
        <View style={styles.form}>
            <Modal
                visible={showScheduleModal}
                transparent
                onRequestClose={() => setShowScheduleModal(!showScheduleModal)}
                animationType="slide"
                hardwareAccelerated
            >
                <View style={styles.scheduleModal}>
                    <View style={styles.scheduleModalBody}>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <TextInput
                                style={styles.scheduleModalInput}
                                keyboardType="numeric"
                                value={scheduleMinutes}
                                onChangeText={(value) => setScheduleMinutes(value)}
                            />
                            <Text style={styles.text}>minute(s)</Text>
                        </View>

                        <View style={styles.scheduleModalButtons}>
                            <TouchableOpacity
                                style={styles.scheduleModalButtonOk}
                                onPress={() => {
                                    addScheduleAlarm(title, description);
                                    setShowScheduleModal(false);
                                }}
                            >
                                <Text style={styles.text}>OK</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.scheduleModalButtonCancel}
                                onPress={() => setShowScheduleModal(false)}
                            >
                                <Text style={styles.text}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
            <Image style={styles.logo} source={require('../../assets/House.png')} />
            <TextInput
                style={
                    isInputFocused.title
                        ? [styles.input, { borderColor: '#5a8ad3', borderWidth: 3 }]
                        : styles.input
                }
                placeholder="Task Title"
                onFocus={() => handleInputFocus('title')}
                onChangeText={(value) => setTitle(value)}
                value={title}
            />
            <TextInput
                style={
                    isInputFocused.description
                        ? [styles.input, { borderColor: '#5a8ad3', borderWidth: 3 }]
                        : styles.input
                }
                placeholder="Task Description"
                onFocus={() => handleInputFocus('description')}
                onChangeText={(value) => setDescription(value)}
                value={description}
                multiline
            />
            <View style={globalStyles.Row}>
                <TouchableOpacity
                    style={styles.scheduleButton}
                    onPress={() => setShowScheduleModal(true)}
                >
                    <MaterialCommunityIcons name="bell-ring-outline" size={32} color="#ffffff" />
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.scheduleButton}
                    onPress={() => navigation.navigate('Camera', { taskId: tasks.length + 1 })}
                >
                    <MaterialCommunityIcons name="camera" size={32} color="#ffffff" />
                </TouchableOpacity>
            </View>
            <View style={styles.colors}>
                {COLORS.map((value, index) => (
                    <TouchableOpacity
                        key={index}
                        style={[
                            {
                                backgroundColor: value,
                                flex: 1,
                                height: 50,
                                justifyContent: 'center',
                                alignItems: 'center',
                            },
                        ]}
                        onPress={() => setColor(value)}
                    >
                        {color === value ? (
                            <MaterialCommunityIcons name="check-bold" color="#ffffff" size={32} />
                        ) : (
                            ''
                        )}
                    </TouchableOpacity>
                ))}
            </View>

            <Button
                label="Create Task   +"
                color="#5a8ad3"
                style={{ width: '100%' }}
                handlePress={() => handleCreateTask()}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    form: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 18,
    },
    input: {
        width: '100%',
        borderWidth: 2,
        borderColor: '#00000050',
        backgroundColor: '#ffffff',
        fontSize: 24,
        marginVertical: 12,
        padding: 16,
        borderRadius: 12,
    },
    logo: {
        width: 120,
        height: 144,
    },
    colors: {
        flexDirection: 'row',
        marginVertical: 5,
    },
    text: {
        color: '#000000',
        fontSize: 18,
        fontFamily: 'Finlandica-Regular',
    },
    scheduleButton: {
        backgroundColor: '#5a8ad3',
        flex: 1,
        padding: 12,
        borderRadius: 12,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    scheduleModal: {
        flex: 1,
        backgroundColor: '#00000099',
        justifyContent: 'center',
        alignItems: 'center',
    },
    scheduleModalBody: {
        width: 300,
        height: 200,
        backgroundColor: '#ffffff',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    scheduleModalInput: {
        borderWidth: 2,
        borderColor: '#00000050',
        minWidth: 64,
        textAlign: 'center',
        borderRadius: 12,
        fontSize: 18,
    },
    scheduleModalButtons: {
        flexDirection: 'row',
    },
    scheduleModalButtonCancel: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#000000',
        borderWidth: 2,
        backgroundColor: 'crimson',
        borderBottomRightRadius: 12,
        paddingVertical: 7,
    },
    scheduleModalButtonOk: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#000000',
        borderWidth: 2,
        backgroundColor: '#5a8ad3',
        borderBottomLeftRadius: 12,
        paddingVertical: 7,
    },
});

export default CreateTask;
