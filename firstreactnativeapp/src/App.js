import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Camera from './screens/Camera';
import Splash from './screens/Splash';
import Tasks from './screens/Tasks';
import CreateTask from './screens/CreateTask';
import UpdateTask from './screens/UpdateTask';
import CompletedTasks from './screens/CompletedTasks';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Provider } from 'react-redux';
import store from './redux/store';

// Initialize a stack navigator
const RootStack = createStackNavigator();

// Initialize a bottom tab navigator
const Tab = createBottomTabNavigator();

// Initialize a material bottom tab navigator
// const Tab = createMaterialBottomTabNavigator();

// Initialize a material top tab navigator
// const Tab = createMaterialTopTabNavigator();

// Initialize a drawer navigator
// const Drawer = createDrawerNavigator()

// Tasks home page -> using the bottom tab navigator
const HomeTabs = () => {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, size, color }) => {
                    let iconName;
                    if (route.name === 'Todo') {
                        iconName = 'clipboard-list-outline';
                        size = focused ? 32 : 28;
                    } else if (route.name === 'Completed') {
                        iconName = 'clipboard-check';
                        size = focused ? 32 : 28;
                    }

                    return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: '#5a8ad3',
                tabBarInactiveBackgroundColor: '#ffffff',
                tabBarLabelStyle: {
                    fontSize: 20,
                    fontWeight: 'bold',
                },
                header: () => null,
                tabBarStyle: {
                    height: 68,
                    paddingVertical: 8,
                    borderTopColor: '#5a8ad3',
                    borderTopWidth: 2,
                },
            })}
        >
            <Tab.Screen name={'Todo'} component={Tasks} />
            <Tab.Screen name={'Completed'} component={CompletedTasks} />
        </Tab.Navigator>
    );
};
const App = () => {
    return (
        <Provider store={store}>
            <NavigationContainer>
                <RootStack.Navigator
                    initialRouteName="Splash"
                    screenOptions={{
                        headerTitleAlign: 'center',
                        headerStyle: {
                            backgroundColor: '#5a8ad3',
                        },
                        headerTintColor: '#ffffff',
                        headerTitleStyle: {
                            fontSize: 25,
                            fontWeight: 'bold',
                        },
                    }}
                >
                    <RootStack.Screen
                        name="Splash"
                        component={Splash}
                        // options={{header: () => null}}
                        options={{
                            title: 'Splash',
                            tabBarLabel: 'Splash',
                            tabBarIcon: ({ color }) => (
                                <MaterialCommunityIcons name="account" color={color} size={26} />
                            ),
                            header: () => null,
                        }}
                        initialParams={{ message: 'Initial message from B' }}
                    />
                    <RootStack.Screen
                        name="Tasks"
                        component={HomeTabs}
                        // options={{header: () => null}}
                    />
                    <RootStack.Screen
                        name="CreateTask"
                        component={CreateTask}
                        options={{
                            title: 'Create Task',
                        }}
                    />
                    <RootStack.Screen
                        name="UpdateTask"
                        component={UpdateTask}
                        options={{
                            title: 'Update Task',
                        }}
                    />
                    <RootStack.Screen
                        name="Camera"
                        component={Camera}
                        options={{
                            title: 'Take Picture',
                        }}
                    />
                </RootStack.Navigator>
            </NavigationContainer>
        </Provider>
    );
};

const styles = StyleSheet.create({
    body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    h1: {
        fontSize: 40,
        fontWeight: 'bold',
        color: '#000',
        marginVertical: 12,
    },
    text: {
        fontSize: 24,
        fontWeight: '400',
        color: '#000',
    },
    button: {
        backgroundColor: '#000000',
        borderRadius: 12,
        padding: 18,
    },
    buttonText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '500',
    },
});

export default App;
