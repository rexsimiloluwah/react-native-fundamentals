import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
    cities: [],
    loading: false,
    status: 'idle',
    error: null,
};

export const fetchCities = createAsyncThunk('cities/fetchCities', async () => {
    const response = await fetch('https://mocki.io/v1/d91d07ce-0d90-4e03-8355-5b0f41bf814d');
    const data = await response.json();
    console.log(data);
    return data;
});

const citiesSlice = createSlice({
    name: 'cities',
    initialState,
    extraReducers: {
        [fetchCities.pending]: (state, action) => {
            state.status = 'loading';
            state.loading = true;
        },
        [fetchCities.fulfilled]: (state, action) => {
            state.loading = false;
            state.status = 'success';
            // Add the fetched cities to the cities array
            state.cities = action.payload;
        },
        [fetchCities.rejected]: (state, action) => {
            state.loading = false;
            state.status = 'fail';
            state.error = action.error.message;
        },
    },
});

export default citiesSlice.reducer;
