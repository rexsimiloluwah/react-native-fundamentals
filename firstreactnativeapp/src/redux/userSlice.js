import { createSlice } from '@reduxjs/toolkit';

export const initialState = {
    email: '',
    password: '',
    count: 0,
};

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setEmail: (state, action) => {
            return {
                ...state,
                email: action.payload,
            };
        },
        setPassword: (state, action) => {
            return {
                ...state,
                password: action.payload,
            };
        },
        getUser: (state, action) => {
            return state;
        },
    },
});

export const { setEmail, setPassword, getUser } = userSlice.actions;
export default userSlice.reducer;
