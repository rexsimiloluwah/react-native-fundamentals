import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    tasks: [],
    taskId: 0,
};

const todoSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        setTasks: (state, action) => {
            return {
                ...state,
                tasks: action.payload,
            };
        },
        setTaskId: (state, action) => {
            return {
                ...state,
                taskId: action.payload,
            };
        },
        updateTask: (state, action) => {
            const { id, data } = action.payload;
            state.tasks[id] = data;
            return state;
        },
        updateCompleted: (state, action) => {
            const { id, value } = action.payload;
            state.tasks[id].done = value;
            return state;
        },
        updateTaskImageUri: (state, action) => {
            const { id, path } = action.payload;
            console.log(id, path);
            state.tasks[id].image = path;
            console.log('updated state: ', state);
            return state;
        },
    },
});

export const { setTasks, setTaskId, updateTask, updateCompleted, updateTaskImageUri } =
    todoSlice.actions;
export default todoSlice.reducer;
