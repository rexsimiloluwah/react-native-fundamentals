import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    count: 0,
};

const countSlice = createSlice({
    name: 'count',
    initialState,
    reducers: {
        incrementCount: (state, action) => {
            return {
                ...state,
                count: state.count + action.payload,
            };
        },
        decrementCount: (state, action) => {
            return {
                ...state,
                count: state.count - action.payload,
            };
        },
    },
});

export const { decrementCount, incrementCount } = countSlice.actions;
export default countSlice.reducer;
