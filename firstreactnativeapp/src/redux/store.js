import { configureStore } from '@reduxjs/toolkit';
import userReducer from './userSlice';
import countReducer from './countSlice';
import citiesReducer from './citiesSlice';
import todoReducer from './todoSlice';

const store = configureStore({
    reducer: {
        user: userReducer,
        count: countReducer,
        cities: citiesReducer,
        todos: todoReducer,
    },
});

export default store;
