import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    Body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 12,
    },
    CustomFont: {
        fontFamily: 'TheNautigal-Bold',
    },
    Button: {
        backgroundColor: '#000000',
        borderRadius: 12,
        padding: 18,
    },
    ButtonText: {
        color: '#fff',
        fontSize: 18,
        fontWeight: '500',
    },
    Input: {
        borderWidth: 2,
        borderColor: '#000',
        width: '100%',
        marginVertical: 12,
        padding: 18,
        borderRadius: 12,
        fontSize: 18,
    },
    WildText: {
        fontSize: 32,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#000',
    },
    Row: {
        flexDirection: 'row',
    },
});
