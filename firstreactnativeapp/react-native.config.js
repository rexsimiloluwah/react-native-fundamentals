module.exports = {
    project: {
        android: {},
    },
    dependencies: {
        'react-native-reanimated': {
            platforms: {
                android: null, // disable Android platform, other platforms will still autolink if provided
                ios: null,
            },
        },
        'react-native-sqlite-storage': {
            platforms: {
                android: {
                    sourceDir:
                        '../node_modules/react-native-sqlite-storage/platforms/android-native',
                    packageImportPath: 'import io.liteglue.SQLitePluginPackage;',
                    packageInstance: 'new SQLitePluginPackage()',
                },
                ios: null,
            },
        },
    },

    assets: ['./assets/fonts'],
};
