// Snapshot testing
// to ensure that the UI does not change abruptly
import React from 'react';
import { FlatList, Text } from 'react-native';
import renderer from 'react-test-renderer';
import Intro from '../src/utils/Intro';

test('renders correctly', () => {
    const tree = renderer.create(<Intro />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('renders the Flatlist component', () => {
    const tree = renderer
        .create(
            <FlatList
                data={['item1', 'item2', 'item3']}
                keyExtractor={(item) => item.toString()}
                renderItem={({ item }) => <Text>{item}</Text>}
            />
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});
